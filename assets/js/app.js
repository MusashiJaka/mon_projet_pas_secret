//@prepros-prepend ../../node_modules/jquery/dist/jquery.min.js
//@prepros-prepend ../../node_modules/bootstrap/dist/js/bootstrap.min.js
//@prepros-prepend ../../node_modules/owl.carousel/dist/owl.carousel.min.js




$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,

    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:2
        }
    }
});

});
